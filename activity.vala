/* activity.vala
 *
 * Copyright 2020 Rasmus Thomsen <oss@cogitri.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Health {

    // TODO: Additional data points which might (!) make sense down the road:
    // * Oxygen saturation & VO2
    // * Body temperature
    // * Blood pressure
    [Flags]
    public enum ActivityDataPoints {
        CALORIES_BURNED,
        DISTANCE,
        DURATION,
        HEART_RATE,
        STEP_COUNT,
    }

    /**
     * A single Activity the user did. 
     */
    public class Activity : GLib.Object {

        public Activities.Enum activity_type {
            get {
                return this.activity_info.type;
            }
            set {
                this.activity_info = Activities.get_values ()[value];
            }
        }
        public GLib.Date date { get; set; }
        public uint32 calories_burned { get; set; }
        public uint32 distance { get; set; }
        public uint32 hearth_rate_avg { get; set; }
        public uint32 hearth_rate_max { get; set; }
        public uint32 hearth_rate_min { get; set; }
        public uint32 minutes { get; set; }
        public uint32 steps { get; set; }

        private Activities.ActivityInfo activity_info;
        private const uint BICYCLING_METERS_PER_MINUTE = 300;
        private const uint HORSE_RIDING_METERS_PER_MINUTE = 260;
        private const uint ROLLER_BLADING_METERS_PER_MINUTE = 240;
        private const uint RUNNING_METERS_PER_MINUTE = 200;
        private const uint SKIING_METERS_PER_MINUTE = 400;
        private const uint SWIMMING_METERS_PER_MINUTE = 160;
        private const uint WALKING_METERS_PER_MINUTE = 90;

        public Activity (
            Activities.Enum activity_type,
            GLib.Date date,
            uint32 calories_burned,
            uint32 distance,
            uint32 hearth_rate_avg,
            uint32 hearth_rate_max,
            uint32 hearth_rate_min,
            uint32 minutes,
            uint32 steps
        ) {
            Object (
                activity_type: activity_type,
                date: date,
                distance: distance,
                hearth_rate_avg: hearth_rate_avg,
                hearth_rate_max: hearth_rate_max,
                hearth_rate_min: hearth_rate_min,
                minutes: minutes,
                steps: steps
            );
        }

        /**
         * If {@link calories_burned} is set, try to autofill the other fields like
         * minutes, steps & distance of the Activity by estimating their values. 
         */
        public void autofill_from_calories () {
            if (this.calories_burned != 0 && ActivityDataPoints.CALORIES_BURNED in this.activity_info.available_data_points) {
                this.minutes = this.calories_burned / Activities.get_values ()[this.activity_type].average_calories_burned_per_minute;

                this.autofill_from_minutes ();
            }
        }

        /**
         * If {@link minutes} is set, try to autofill the other fields like
         * calories_burned, steps & distance of the Activity by estimating their values. 
         */
        public void autofill_from_minutes () {
            if (this.minutes != 0 && ActivityDataPoints.DURATION in this.activity_info.available_data_points) {
                this.calories_burned = this.activity_info.average_calories_burned_per_minute * this.minutes;

                switch (this.activity_type) {
                    case Activities.Enum.BICYCLING:
                        this.distance = BICYCLING_METERS_PER_MINUTE * this.minutes;
                        break;
                    case Activities.Enum.HORSE_RIDING:
                        this.distance = HORSE_RIDING_METERS_PER_MINUTE * this.minutes;
                        break;
                    case Activities.Enum.HIKING:
                    case Activities.Enum.WALKING:
                        this.distance = WALKING_METERS_PER_MINUTE * this.minutes;
                        break;
                    case Activities.Enum.ROLLERBLADING:
                        this.distance = ROLLER_BLADING_METERS_PER_MINUTE * this.minutes;
                        break;
                    case Activities.Enum.RUNNING:
                    case Activities.Enum.TRACK_AND_FIELD:
                        this.distance = RUNNING_METERS_PER_MINUTE * this.minutes;
                        break;
                    case Activities.Enum.SKIING:
                        this.distance = SKIING_METERS_PER_MINUTE * this.minutes;
                        break;
                    case Activities.Enum.SWIMMING:
                        this.distance = SWIMMING_METERS_PER_MINUTE * this.minutes;
                        break;
                    // Other activities don't have ActivityDataPoints.DISTANCE
                    default:
                        break;
                }

                switch (this.activity_type) {
                    case Activities.Enum.WALKING:
                    case Activities.Enum.HIKING:
                        this.steps = this.minutes * 100;
                        break;
                    case Activities.Enum.RUNNING:
                        this.steps = this.minutes * 150;
                        break;
                    // Other activities don't have ActivityDataPoints.STEPS
                    default:
                        break;
                }
            }
        }

        /**
         * If {@link distance} is set, try to autofill the other fields like
         * calories, steps & minutes of the Activity by estimating their values. 
         */
        public void autofill_from_distance () {
            if (distance != 0 && ActivityDataPoints.DISTANCE in this.activity_info.available_data_points) {
                switch (this.activity_type) {
                    case Activities.Enum.BICYCLING:
                        this.minutes = this.distance / BICYCLING_METERS_PER_MINUTE;
                        break;
                    case Activities.Enum.HORSE_RIDING:
                        this.minutes = this.distance / HORSE_RIDING_METERS_PER_MINUTE;
                        break;
                    case Activities.Enum.HIKING:
                    case Activities.Enum.WALKING:
                        this.minutes = this.distance / WALKING_METERS_PER_MINUTE;
                        break;
                    case Activities.Enum.ROLLERBLADING:
                        this.minutes = this.distance / ROLLER_BLADING_METERS_PER_MINUTE;
                        break;
                    case Activities.Enum.RUNNING:
                    case Activities.Enum.TRACK_AND_FIELD:
                        this.minutes = this.distance / RUNNING_METERS_PER_MINUTE;
                        break;
                    case Activities.Enum.SKIING:
                        this.minutes = this.distance / SKIING_METERS_PER_MINUTE;
                        break;
                    case Activities.Enum.SWIMMING:
                        this.minutes = this.distance / SWIMMING_METERS_PER_MINUTE;
                        break;
                    // Other activities don't have ActivityDataPoints.DISTANCE
                    default:
                        break;
                }

                this.calories_burned = this.minutes * this.activity_info.average_calories_burned_per_minute;

                switch (this.activity_type) {
                    case Activities.Enum.WALKING:
                    case Activities.Enum.HIKING:
                    case Activities.Enum.RUNNING:
                        this.steps = (uint32) (this.distance * 1.4);
                        break;
                    // Other activities don't have ActivityDataPoints.STEPS
                    default:
                        break;
                }
            }
        }

        /**
         * If {@link steps} is set, try to autofill the other fields like
         * minutes, calories_burned & distance of the Activity by estimating their values. 
         */
        public void autofill_from_steps () {
            if (this.steps != 0 && ActivityDataPoints.STEP_COUNT in this.activity_info.available_data_points) {
                switch (this.activity_type) {
                case Activities.Enum.WALKING:
                    this.minutes = this.steps / 100;
                    this.distance = this.minutes * WALKING_METERS_PER_MINUTE;
                    break;
                case Activities.Enum.HIKING:
                    this.minutes = this.steps / 80;
                    this.distance = this.minutes * WALKING_METERS_PER_MINUTE;
                    break;
                case Activities.Enum.RUNNING:
                    this.minutes = this.steps / 150;
                    this.distance = this.minutes * RUNNING_METERS_PER_MINUTE;
                    break;
                // Other activities don't have ActivityDataPoints.STEPS
                default:
                    break;
                }

                this.calories_burned = this.activity_info.average_calories_burned_per_minute * this.minutes;
            }
        }

    }

    /**
     * A class containing information about different activities. The contained Enum contains all supported activities.
     */
    public class Activities : GLib.Object {
        public enum Enum {
            BASKETBALL,
            BICYCLING,
            BOXING,
            DANCING,
            FOOTBALL,
            GOLF,
            HIKING,
            HOCKEY,
            HORSE_RIDING,
            OTHER_SPORTS,
            ROLLERBLADING,
            RUNNING,
            SKIING,
            SOCCER,
            SOFTBALL,
            SWIMMING,
            TENNIS,
            TRACK_AND_FIELD,
            VOLLEYBAL,
            WALKING,
        }

        public struct ActivityInfo {
            Activities.Enum type;
            string name;
            ActivityDataPoints available_data_points;
            // As per https://keisan.casio.com/menu/system/000000000140
            uint32 average_calories_burned_per_minute;
            // As per https://www.arhs-nc.org/live-healthy/data/StepConversionChart.pdf
            // uint32 steps_per_minute;
        }

        public static ActivityInfo? get_info_by_name (string name) {
            foreach (var x in Activities.get_values ()) {
                if (x.name == name) {
                    return x;
                }
            }

            return null;
        }

        public static ActivityInfo[] get_values () {
            return {
                {Enum.BASKETBALL, "Basketball", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 6},
                {Enum.BICYCLING, "Bicycling", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE | ActivityDataPoints.DISTANCE, 10},
                {Enum.BOXING, "Boxing", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 7},
                {Enum.DANCING, "Dancing", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 8},
                {Enum.FOOTBALL, "Football", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 3},
                {Enum.GOLF, "Golf", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION, 4},
                {Enum.HIKING, "Hiking", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.STEP_COUNT | ActivityDataPoints.DISTANCE | ActivityDataPoints.HEART_RATE | ActivityDataPoints.DURATION, 8},
                {Enum.HOCKEY, "Hockey", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 10},
                {Enum.HORSE_RIDING, "Horse Riding", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE | ActivityDataPoints.DISTANCE, 5},
                {Enum.OTHER_SPORTS, "Other Sports", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 9},
                {Enum.ROLLERBLADING, "Rollerblading", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE | ActivityDataPoints.DISTANCE, 10},
                {Enum.RUNNING, "Running", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.STEP_COUNT | ActivityDataPoints.DISTANCE | ActivityDataPoints.HEART_RATE | ActivityDataPoints.DURATION, 15},
                {Enum.SKIING, "Skiing", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE | ActivityDataPoints.DISTANCE, 12},
                {Enum.SOCCER, "Soccer", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 8},
                {Enum.SOFTBALL, "Softball", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 5},
                {Enum.SWIMMING, "Swimming", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE | ActivityDataPoints.DISTANCE, 12},
                {Enum.TENNIS, "Tennis", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 6},
                {Enum.TRACK_AND_FIELD, "Track And Field", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE | ActivityDataPoints.DISTANCE | ActivityDataPoints.STEP_COUNT, 5},
                {Enum.VOLLEYBAL, "Volleyball", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.DURATION | ActivityDataPoints.HEART_RATE, 4},
                {Enum.WALKING, "Walking", ActivityDataPoints.CALORIES_BURNED | ActivityDataPoints.STEP_COUNT | ActivityDataPoints.DISTANCE | ActivityDataPoints.HEART_RATE | ActivityDataPoints.DURATION, 5},
            };
        }
    }
}
